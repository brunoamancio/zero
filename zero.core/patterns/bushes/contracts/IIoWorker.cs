﻿using zero.core.patterns.heap;

namespace zero.core.patterns.bushes.contracts
{
    public interface IIoWorker : IIoHeapItem
    {
    }
}
